                                      ###############  MODULE 3  ################

                    #####  CREATING DATAFRAME FOR MATCHING FORECASTED SKU's WITH THE PLAN SKU's  ######

start_time<-Sys.time()
newset_df<-data.frame()
for(i in 1:nrow(dtry))
{
new_set<-week13[week13$SKU==dtry$SKU[i],]
newset_df<-rbind(newset_df,new_set)
}


#### MODIFYING COLUMN NAMES ######

#colnames(dtry)[grep("plan",colnames(dtry))]<-paste0("new_Plan_",c(1:13))
colnames(dtry)[grep("Plan",colnames(dtry))]<-paste0("new_Plan_",c(1:13))
#colnames(dtry)[grep("close",colnames(dtry))]<-paste0("new_close_",c(1:13))
colnames(dtry)[grep("Close",colnames(dtry))]<-paste0("new_close_",c(1:13))
#colnames(dtry)[grep("cover",colnames(dtry))]<-paste0("new_cover_",c(1:13))
colnames(dtry)[grep("Cover",colnames(dtry))]<-paste0("new_cover_",c(1:13))

##### MERGING DATAFRAMES ######

new_merge_df<-merge(newset_df,dtry[,c(1,grep("new_Plan",colnames(dtry)))],by = "SKU",all.x=T)
new_merge_df<-merge(new_merge_df,dtry[,c(1,grep("new_close",colnames(dtry)))],by = "SKU",all.x=T)
new_merge_df<-merge(new_merge_df,dtry[,c(1,grep("new_cover",colnames(dtry)))],by = "SKU",all.x=T)

new_merge_df[,grep("plan",colnames(new_merge_df))]<-new_merge_df[,grep("new_Plan",colnames(new_merge_df))]
close.out_index<-grep("close.out",colnames(new_merge_df))
close.out_index<-close.out_index[-1]
new_merge_df[,close.out_index]<-new_merge_df[,grep("new_close",colnames(new_merge_df))]
new_merge_df[,grep("cover.out",colnames(new_merge_df))]<-new_merge_df[,grep("new_cover",colnames(new_merge_df))]

new_merge_df<-new_merge_df[,-grep("new_Plan",colnames(new_merge_df))]
new_merge_df<-new_merge_df[,-grep("new_close",colnames(new_merge_df))]
new_merge_df<-new_merge_df[,-grep("new_cover",colnames(new_merge_df))]
week13<-week13[!(week13$SKU %in% new_merge_df$SKU),]
week13<-rbind(week13,new_merge_df)

print(Sys.time()-start_time)
########  OUTPUT OF THIS MODULE WILL BE INPUT FOR NEXT MODULE 4 #########
#######################   END   ##########################
summ<-c()
plan_index1<-grep("plan",colnames(week13))
for(i in 1:length(plan_index1))
{
  summ[i]<-sum(week13[,plan_index1[i]],na.rm=T)
 
}
print(summ)

