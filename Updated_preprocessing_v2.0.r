#setwd("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week33_45\\new_format_inputs(long)\\wk30")
#setwd("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week33_45\\new_format_inputs(long)\\wk31")
#setwd("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week33_45\\new_format_inputs(long)\\wk32")
#setwd("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week33_45\\new_format_inputs(long)\\wk33")
setwd("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week33_45\\new_format_inputs(long)\\wk34")
getwd()

#install.packages("dplyr")
library(dplyr)
library(tidyr)
library(reshape2)
#data_preprocessing<-maml.mapInputPort(1)
data_preprocessing<-read.csv("Intermediate Data_.csv",stringsAsFactors = F)
colnames(data_preprocessing)[c(1,12)]<-c("Date","costPerTon")
data_preprocessing$costPerTon[is.na(data_preprocessing$costPerTon)]<-999
data_preprocessing$Target.Cover.Norm<-ifelse(data_preprocessing$costPerTon  == 0,0,data_preprocessing$Target.Cover.Norm)
data_preprocessing$Target.Cover.Norm[is.na(data_preprocessing$Target.Cover.Norm)]<-2.7
data_preprocessing$costPerTon[which(data_preprocessing$costPerTon == 999)]<-NA

#################################MOQ_replacement########

unique_groups<-unique(data_preprocessing$Group.Number)
data_preprocessing$MOQ<-as.numeric(data_preprocessing$MOQ)
data_preprocessing_copy_1<-data.frame()
for(i in 1:length(unique_groups))
{
  #print(i)
  sub_g<-subset(data_preprocessing,data_preprocessing$Group.Number == unique_groups[i])
  moq_check<-which(!(is.na(sub_g$MOQ)))
  if(length(moq_check) > 1)
  {
    sub_g$MOQ<-sub_g[moq_check[1],8]
  }
  data_preprocessing_copy_1<-rbind(data_preprocessing_copy_1,sub_g)
}

DP_na<-data_preprocessing[which(is.na(data_preprocessing$Group.Number)),]
data_preprocessing_copy_1<-rbind(data_preprocessing_copy_1,DP_na)
data_preprocessing_copy_1$MOQ[is.na(data_preprocessing_copy_1$MOQ)]<-" "
data_preprocessing<-data_preprocessing_copy_1
current_13weeks<-data_preprocessing
###############################

#data_preprocessing$Target.Cover.Norm[is.na(data_preprocessing$Target.Cover.Norm)]<-2.7
#a<-as.Date(data_preprocessing[,1],"%Y-%m-%d","%d-%m-%Y")
#data_preprocessing[,1]<-format(as.Date(data_preprocessing[,1]), "%d-%m-%Y")

#min_quantity<-unique(data_preprocessing[,c("Material","MinVolProd")])

data_preprocessing1<-data_preprocessing %>% unite(Date_Week,c(Date,Week))
colnames(data_preprocessing1)[c(4,5,13:18)]<-c("SkuType","Group","fcst","planvalues","close.In","4wksf","cover","cover.In")

#(unique(data_preprocessing1$Date_Week))
col<-c("fcst","planvalues","close.In","4wksf","cover","cover.In")
list1<-list()
counter=1

#colnames(data_preprocessing1)[c(4,5)]<-c("SkuType","Group")
for (i in col)
{
  
  a<-assign(paste0("df_",i),data_preprocessing1[,c("Material","SkuType","Group","Date_Week",i)])
  a[,"Date_Week"]<-paste0(i,"_",a[,"Date_Week"])
  b<-assign(paste0("df_",i,"_1"),dcast(a,Material+SkuType+Group~Date_Week,value.var=i))
  
  list1[[counter]]=b
  counter=counter+1
  
}

df<-data_preprocessing1[,c("Material","SkuType","Group")]
df<-df[!duplicated(df),]
#colnames(df)<-"Material"
for ( i in 1:6)
{
  df<-merge(df,list1[[i]],by=c("Material","SkuType","Group"))
}
#a<-as.data.frame(strsplit(as.character(colnames_df$`colnames(df)[4:length(colnames(df))]`),'_',fixed = T))
colnames_df<-as.data.frame(colnames(df)[4:length(colnames(df))])
colnames_df<-as.data.frame(do.call(rbind,strsplit(as.character(colnames_df$`colnames(df)[4:length(colnames(df))]`),'_',fixed = T)))
colnames(colnames_df)<-c("Name","Date","Week")
colnames_df$Date<- format(as.Date(colnames_df$Date,"%d-%m-%Y"),"%d-%m-%Y")
colnames_df<-separate(colnames_df,col=Date,into = c("Day","Month","Year"),remove = F)
colnames_df<-colnames_df[with(colnames_df,order(Year,Month,Day)),]
#colnames_df<-colnames_df[order(colnames_df$Date),]
colnames_df1<-paste0(colnames_df$Name,"_",colnames_df$Date,"_",colnames_df$Week)
colnames_df0<-c("Material","SkuType","Group")
df<-df[,c(colnames_df0,colnames_df1)]



MOQ_DATA<-data_preprocessing1[,c(3,7)]
unique_MOQDATA<-unique(MOQ_DATA)
df<-merge(df,unique_MOQDATA,by="Material",all.x = T)
df$pre_close<-df[,48]#pre_close


sd_1<-as.character(as.Date(substr(colnames(df)[4],6,15),"%d-%m-%Y"))
month<-required_week_info[c(which(required_week_info$Dates == sd_1):(which(required_week_info$Dates == sd_1)+20)),]
month<-as.vector(month[2])
month<-unlist(month)
month_rep<-rep(month,each=6)
colnames(df)[4:(ncol(df)-2)]<-paste0(colnames(df)[4:(ncol(df)-2)],"/",month_rep)
start_date_month<-substr(colnames(df)[52],22,23)

#month<-substr(start_date,6,7)
hist_data<-df[,c(1:51)]
df<-df[,-c(4:51)]
df<-df[,c(1:3,82,83,4:81)]
plan_data<-hist_data[,c(1,grep("planvalues",colnames(hist_data)))]

Pre_Plan<-which(substr(colnames(plan_data),28,29) == start_date_month)
#Pre_Plan<-c(3,4)
if(length(Pre_Plan) > 0 )
{
  Pre_Plan_data<-plan_data[,c(1,Pre_Plan)]
  
  colnames(Pre_Plan_data)[-1]<-paste0("Plan","_",c(substr(colnames(Pre_Plan_data)[-1],23,26)))
  df<-merge(df,Pre_Plan_data,by="Material",all.x = T)
  cover<-grep("cover.In",colnames(df))
    
  #df<-df[,c(1:5,((ncol(df)-2)+1) : ncol(df),6:(ncol(df)-2))]
  df<-df[,c(1:5,(cover[length(cover)]+1) : ncol(df),6:cover[length(cover)])]
}

#output dataframe <- df 


