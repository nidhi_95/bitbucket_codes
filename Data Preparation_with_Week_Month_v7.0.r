#select statement is removed.                                  ########### MODULE 1 ############
#avoiding manual inputs                       ########  PLAN CALCULATION BASED ON COVER NORM  #########
#input taken from preprocessing
######## REQUIRED LIBRARIES ########
start_time<-Sys.time()
#library(plyr)
library(dplyr)
library(lubridate)



### SELECT THE SOURCE FILE ###

#source_data<-read.csv("source_file.csv",as.is=T,stringsAsFactors = F)
source_data<-df
colnames(source_data)[c(1,2)]<-c("SKU","SKU_TYPE")

### EXTRACTING START DATE AND END DATE ###

# header <- scan("source_file.csv", nlines = 1, what = character())
# header1<-strsplit(header,",")
# header1<-unlist(header1)

#colnames(source_data)<-source_data[1,]
#source_data<-source_data[-1,]
#start<-grep("forecast",colnames(source_data))[1]
fc_index<-grep("fcst",colnames(source_data))
start<-substr(colnames(source_data)[fc_index[1]],6,15)
end<-substr(colnames(source_data)[fc_index[length(fc_index)]],6,15)
#end<-grep("forecast",colnames(source_data))[length(grep("forecast",colnames(source_data)))]


start_date<-as.Date(start,"%d-%m-%Y")
end_date<-as.Date(end,"%d-%m-%Y")

#check later where it is used
source_data_1<-source_data
start_date_2<-start_date
end_date_2<-end_date

cover_values<-source_data[,c(1,grep("cover.In",colnames(source_data)))]
  #cover_values<-as.data.frame(apply(cover_values[,c(1,plan_index1)],2,as.numeric))
# summ<-c()
# plan_index1<-grep("cover.In",colnames(cover_values))
# for(i in 1:length(plan_index1))
# {
#   summ[i]<-sum(cover_values[,plan_index1[i]],na.rm=T)
# 
# }
# try<-which(summ == 0)
# 
# if(length(try)>0)
# {
# cover_values[,(which(summ == 0)+1)]<-2.7
# }

group_mapping<-source_data[,c(1,2,3)]
group_mapping$INTERCHANGE_SKU_NUMBER<-0
group_mapping<-group_mapping[,c(1,2,4,3)]
source_data<-source_data[,-c(grep("cover.In",colnames(source_data)))]
source_data<-source_data[,-c(grep("Group",colnames(source_data)),grep("SKU_TYPE",colnames(source_data)))]
source_data<-source_data[,c(2,1,3:ncol(source_data))]
colnames(source_data)[3]<-"close"
#################################################################################
colnames<-c("fcst","plan_values","close.In","4wksf","cover.In")

date.seq <- seq.Date((start_date),(end_date), by="week")


########## FUNCTION FOR  WEEK - MONTH - IDENTIFICATION  ##########
week_month_identifier<-function(dateseq)
{
  month_identifier<-function(weekNum)
  {
    startDate=as.Date(paste(2018, weekNum-1, 1, sep="-"), "%Y-%U-%u")
    startDate
    endDate= startDate + days(7)
    myDates <-seq(from = startDate, to = endDate, by = "days")
    weekstart=myDates[wday(myDates)==5]
    weekstart
    m=month(weekstart)
    n=month(startDate)
    
    monthname<-ifelse(month(weekstart)==month(startDate), toString(month(startDate, label = TRUE)), toString(month(endDate, label = TRUE)))
    monthnumeber<-match(monthname,month.abb)
    return(monthnumeber)
  }
  
  monthvec<-c()
  monthvec1<-c()
  for(i in 1:length(dateseq))
  {
    
    monthvec[i]=month_identifier(week(dateseq[i]))
    monthvec1[i]<-ifelse(monthvec[i]<10,paste0("0",monthvec[i]),monthvec[i])
  }
  
  return(monthvec1)
}

###########################################################################################################################

rep_colnames<-rep(colnames,times= length(date.seq))
rep_dates<-rep(date.seq,each=length(colnames))


colnames_1<-paste0(rep_colnames,"_",rep_dates,"/",week_month_identifier(rep_dates))


colnames(source_data)[1:3]<-c("MOQ","SKU","close.out0")
source_data<-source_data[,c(1:3,grep("fcst",colnames(source_data))[1]:ncol(source_data))]
colnames(source_data)[4:ncol(source_data)]<-colnames_1
 

### SELECT THE FILE CONTAINING COVER VALUES  ###


#cover_values<-read.csv("Stock cover target.csv")

cover_names<-rep("cover.In",(ncol(cover_values)-1))
rep_dates<-date.seq

#paste cover with months

colnames_1<-paste0(cover_names,"_",rep_dates,"/",week_month_identifier(rep_dates))


colnames(cover_values)[1]<-"SKU"
colnames(cover_values)[2:ncol(cover_values)]<-colnames_1
#cover_values[is.na(cover_values)]<-2.7  

source_data$Group<-0

input_values<-source_data[,c("SKU","MOQ","Group","close.out0")]
######### PERFORMING COMPUTATIONS MONTH WISE ########

unique_months<-unique(week_month_identifier(date.seq))
months<-week_month_identifier(date.seq)
for(i in 1:length(unique_months))

{
  
  date.seq_1<-months[which(months == unique_months[i])]
  num<-length(date.seq_1)
  append_date<-date.seq[c(1:num)]
  date.seq<-date.seq[-c(1:num)]
  
  #fourwksfcst<-select(source_data,"SKU",contains("4wksf"))
  fourwksfcst<-source_data[,c((which(colnames(source_data) == "SKU")),grep("4wksf",colnames(source_data)))]
  fourwksfcst<-fourwksfcst[,c(1,which(substr(colnames(fourwksfcst),18,19) == unique_months[i]))]
  
  
  fcst<-source_data[,c((which(colnames(source_data) == "SKU")),grep("fcst",colnames(source_data)))]
  fcst<-fcst[,c(1,which(substr(colnames(fcst),17,18) == unique_months[i]))]
  
  cover<-cover_values[,c((which(colnames(cover_values) == "SKU")),grep("cover.In",colnames(cover_values)))]
  cover<-cover[,c(1,which(substr(colnames(cover),21,22) == unique_months[i]))]
  
  
 
  numcol.a<-ncol(fourwksfcst)-1
  monthly_dataframe<-merge(fourwksfcst,cover,by="SKU",all.x = T)#change made (made it as left join) coz priority is given to the skus present in cover values sheet
  monthly_dataframe<-merge(monthly_dataframe,fcst,by="SKU",all.x=T)#change made (made it as left join)
  monthly_dataframe<-merge(input_values,monthly_dataframe,by="SKU",all.y=T)#change made(made it as right join)
  monthly_dataframe_1<-monthly_dataframe
  monthly_dataframe[is.na(monthly_dataframe)]<-0
 
  monthly_dataframe<-as.data.frame(lapply(monthly_dataframe,as.numeric))
  colnames(monthly_dataframe)<-colnames(monthly_dataframe_1)
  if(i>1)
  {
    close.out<-grep("close.out",colnames(monthly_dataframe_2))
    len<-length(close.out)
    close.out<-monthly_dataframe_2[,c(1,close.out[len])]
    monthly_dataframe<-merge(monthly_dataframe,close.out,by="SKU")
    monthly_dataframe[,4]<-monthly_dataframe[,ncol(monthly_dataframe)]
    colnames(monthly_dataframe)[4]<-"close.out0"
    monthly_dataframe<-monthly_dataframe[,-ncol(monthly_dataframe)]
  }

#######  CLOSE CALCULATION BASED ON COVER NORM(close = cover_norm*4wksfcst/4) #######

for(k in 1:numcol.a)
  {
    name_close<-paste0("close.In",k)
    fourwksf.in<-grep("4wksf",colnames(monthly_dataframe))
    cover.in<-grep("cover.In",colnames(monthly_dataframe))
    
    monthly_dataframe[name_close]<-round(monthly_dataframe[,cover.in[k]]*(monthly_dataframe[,fourwksf.in[k]]/4),6)
    
  }
  




for(l in 1:numcol.a)
  {
    name_plan<-paste0("plan",l)
    name_close<-paste0("close.out",l)
    close.out<-grep("close.out",colnames(monthly_dataframe))
    len<-length(close.out)
    close.in<-grep("close.In",colnames(monthly_dataframe))
    fcst<-grep("fcst",colnames(monthly_dataframe))
    plan<-grep("plan",colnames(monthly_dataframe))
    
  ######   PLAN CALCULATION(plan = prev.close-fcst+plan)###########
    
    close<-monthly_dataframe[,close.in[l]]-monthly_dataframe[,close.out[len]]
    monthly_dataframe[name_plan]<-round(pmax(close+monthly_dataframe[,fcst[l]],0),6)
    
    plan<-grep("plan",colnames(monthly_dataframe))
    len<-length(plan)
    
  ######  CLOSE CALCULATION BASED ON NEWLY GENERATED PLAN VALUES (close = prev.close - fcst + plan )  
    monthly_dataframe[name_close]<-round(monthly_dataframe[,close.out[len]]-monthly_dataframe[,fcst[l]]+monthly_dataframe[,plan[len]],6)
    
  }

##############     COVER  CALCULATION     ##############  
  
for(m in 1:numcol.a)
  {
    name_cover.out<-paste0("cover.out",m)
    close.out<-grep("close.out",colnames(monthly_dataframe))
    fourwksf<-grep("4wksf",colnames(monthly_dataframe))
    monthly_dataframe[name_cover.out]<-(monthly_dataframe[,close.out[m+1]]/monthly_dataframe[,fourwksf[m]])*4
  }
  
  monthly_dataframe_2<-monthly_dataframe

##############          SELECT THE GROUP MAPPING FILE         ###############
  
  
  #group_mapping<-read.csv("mapping.csv")
  monthly_dataframe_3<-merge(monthly_dataframe,group_mapping,by="SKU",all.x = T)
  monthly_dataframe_3[,3]<-monthly_dataframe_3[,ncol(monthly_dataframe_3)]
  monthly_dataframe_3<-monthly_dataframe_3[,c(1:4,(ncol(monthly_dataframe_3)-2):(ncol(monthly_dataframe_3)-1),5:(ncol(monthly_dataframe_3)-3))]
  colnames(monthly_dataframe_3)[3]<-"Group"
  if(i==1){
    week13<-monthly_dataframe_3[,c(1:6)]}

  trial<-monthly_dataframe_3[monthly_dataframe_3$SKU_TYPE == "make to order",]
  trial[,grep("plan",colnames(monthly_dataframe_3))]<-trial[,grep("fcst",colnames(monthly_dataframe_3))]
  trial_1<-monthly_dataframe_3[monthly_dataframe_3$SKU_TYPE != "make to order",]
  monthly_dataframe_3<-rbind(trial_1,trial)
  colnames(monthly_dataframe_3)[4]<-"pre_close"
  monthly_dataframe_3<-monthly_dataframe_3[,c(1:6,grep("cover.In",colnames(monthly_dataframe_3)),grep("4wksf",colnames(monthly_dataframe_3)),grep("fcst",colnames(monthly_dataframe_3)),grep("close.In",colnames(monthly_dataframe_3)),grep("plan",colnames(monthly_dataframe_3)),grep("close.out",colnames(monthly_dataframe_3)),grep("cover.out",colnames(monthly_dataframe_3)))]
  colnames(monthly_dataframe_3)[4]<-"close.out0"
  #monthly_dataframe_3<-select(monthly_dataframe_3,c(1:6),contains("cover.In"),contains("4wksf"),contains("fcst"),contains("close.In"),contains("plan"),contains("close.out"),contains("cover.out"))
  date.seq_new<-rep(date.seq_1,times = 7)
  date.seq_apend<-rep(append_date,times = 7)
 
  colnames<-c("cover.In","4wksf","fcst","close.In","plan","close.out","cover.out")
  colnames_monthly<-rep(colnames,each = length(fourwksf))
  
  colnames_new<-paste0(colnames_monthly,"_",date.seq_apend,"/",date.seq_new)
  
  colnames(monthly_dataframe_3)[7:length(monthly_dataframe_3)]<-colnames_new
  
  week13<-merge(week13,monthly_dataframe_3[,c(1,7:length(monthly_dataframe_3))],by = "SKU",all.x = T)

  nam <- paste("month_new",i)
  assign(nam,monthly_dataframe_3)
  
  
}
print(Sys.time()-start_time)



#### OUTPUT OF THIS MODULE 1 WILL BE INPUT FOR THE NEXT MODULE 2 ####
###################          END                    #################


summ<-c()
plan_index1<-grep("plan",colnames(week13))
for(i in 1:length(plan_index1))
{
  summ[i]<-sum(week13[,plan_index1[i]],na.rm=T)

}
print(summ)


