library(dplyr)
library(tidyr)


##AZURE##
# final_data2test<-maml.mapInputPort(1)
# current_13weeks<-maml.mapInputPort(2)
# final_data2test[,1]<-format(as.Date(final_data2test[,1]), "%d-%m-%Y")
# current_13weeks[,1]<-format(as.Date(current_13weeks[,1]), "%d-%m-%Y")
# install.packages("src/urca_1.3-0.zip", lib = ".", repos = NULL, verbose = TRUE)
# install.packages("src/vars_1.5-2.zip", lib = ".", repos = NULL, verbose = TRUE)
# library(urca, lib.loc=".", verbose=TRUE)
# library(vars, lib.loc=".", verbose=TRUE)  
##AZURE##

final_data2test<-read.csv("Historical Data - Final Transformed Format_.csv",stringsAsFactors = F)
#unique_dates<-sort(unique(final_data2test$Date))
#unique_dates[length(unique_dates)]

#current_13weeks<-read.csv("Intermediate Data_.csv",stringsAsFactors = F)
#current_13weeks<-data_preprocessing
#unique_dates<-unique(current_13weeks$Date)
#last_date<-unique_dates[length(unique_dates)]

#which()


current_13weeks_copy<-current_13weeks[order(current_13weeks$Week,current_13weeks$Date),]
Unique_Date_week<-unique(current_13weeks_copy[,c("Date","Week")])
row.names(Unique_Date_week)<-NULL
Dates_hist_from_inter_file<-Unique_Date_week$Date
#Latest_hist<-current_13weeks_copy[current_13weeks_copy$Date %in% Dates_hist_from_inter_file,]
Latest_hist<-current_13weeks[,-c(8:13,19)]
#final_data2test$Date-Dates_hist_from_inter_file
#final_data2test_check<-final_data2test[final_data2test$Date %in% Dates_hist_from_inter_file,]
final_data2test<-final_data2test[!final_data2test$Date %in% Dates_hist_from_inter_file,]#old hist removal
final_data2test<-rbind(final_data2test,Latest_hist)
add<-final_data2test
##
#current_13weeks_copy<-current_13weeks[order(current_13weeks$Week,current_13weeks$Date),]
#Last_Date<-as.character(current_13weeks_copy$Date[nrow(current_13weeks_copy)])
##

colnames(final_data2test)[c(5,6)]<-c("SkuType","GroupNumber")
colnames(current_13weeks)[c(5,6,19)]<-c("SkuType","GroupNumber","TargetCoverNorm")
final_data2test1<-final_data2test[final_data2test$SkuType=="Regular",]
final_data2test2<-separate(final_data2test1,col=Date,into = c("Day","Month","Year"),remove = F)
final_data2test1<-(final_data2test2[with(final_data2test2,order(Year,Month,Day)),])
#which(is.na(add$Plan))


colnames(final_data2test1)[1]<-"Date"
colnames(current_13weeks)[1]<-"Date"
current_13weeks1<-current_13weeks[,c("Date","Week","Material","TargetCoverNorm")]
current_13weeks1$TargetCoverNorm[is.na(current_13weeks1$TargetCoverNorm)]=2.7
data_with_stock<-merge(final_data2test1,current_13weeks1,by=c("Material","Date","Week"),all.x=T)
data_with_stock<-data_with_stock[with(data_with_stock,order(Year,Month,Day)),]
data_with_stock$Week<-NULL
data_with_stock$Plant<-NULL
colnames(data_with_stock)[14]=c("target_cover")
row.names(data_with_stock)<-NULL

#match_last_date<-which(data_with_stock$Date == Last_Date)
#data_with_stock<-data_with_stock[-c((match_last_date[length(match_last_date)]+1):nrow(data_with_stock)),]

SKU<-unique(data_with_stock$Material)

#SKU<-SKU[-c(66,67)]
#SKU<-SKU[-c(which(SKU == 67627638),which(SKU == 67627640))]
sku_list<-list()
a=1
for(i in SKU)
{
  #print (i)
  data_with_stock_persku=data_with_stock[data_with_stock$Material==i,]
  sku_list[[a]]<-data_with_stock_persku
  a=a+1
}

#sku_list1<-lapply(sku_list,f(x) x[with(x,order(Year,Month,Day)),])
#View(sku_list[[1]])

#------------------------------------------------
#install.packages("forecast")
library(forecast)
#install.packages("vars")
library(vars)
#test<-as.data.frame(sku_list[66])
#df_timeseries<-test 
Var_model<-function(df_timeseries)
{
  k=13 # Forecast period
  file.df <-df_timeseries
  file.df[,c(9:14)]<-apply(file.df[,c(9:14)],2,as.numeric)
  while(k>0){
    #print(i)
    file.df1 <- head(file.df,-k)
    file.df1[is.na(file.df1)] <- 0.0 
    dac <- c("Plan","Close","Fcst4wk","Fcst" ) 
    x=file.df1[,dac]
    diff.stationary <- ndiffs(x[, "Close"], alpha = 0.1, test = c("kpss", "adf", "pp"), max.d = 2)
    if(diff.stationary>=1){
      diff.stationary <- as.integer(diff.stationary)  
      d.x1 = diff(x[, "Plan"], differences = diff.stationary)
      d.x2 = diff(x[, "Close"], differences = diff.stationary)
      
    }  else{
      d.x1 = x[, "Plan"]
      d.x2 = x[, "Close"]
      
    }
    dx = cbind(d.x1, d.x2)
    var.result <- VARselect(dx, lag.max = 10, type = "both")
    p1 <- var.result$selection[[1]]
    p2 <- var.result$selection[[2]]
    p3 <- var.result$selection[[3]]
    p4 <- var.result$selection[[4]]
    p.array <- c(p1,p2,p3,p4)
    p.table <- table(p.array)
    p <- names(p.table)[which.max(p.table)]
    var = VAR(dx, p=as.numeric(p))
    prd <- predict(var, n.ahead = 1, ci = 0.95, dumvar = NULL)
    Fcst.Close <- prd$fcst$d.x2[1,1]
    Material <- file.df$SKU[1]
    Close.Forecast <- Fcst.Close
    Prev.Close <- tail(file.df1$Close,1)
    FCST4WEEK <-  head(tail(file.df$Fcst4wk,k),1)
    Target.Cover <- head(tail(file.df$target_cover,k),1)
    Target.Cover1=head(tail(file.df$target_cover*1.0001,k),1)
    Threshold.Close <- (FCST4WEEK/4)*Target.Cover
    
    Threshold.Close1 <- (FCST4WEEK/4)*Target.Cover1
    
    Computed.Close <- max(Threshold.Close,Close.Forecast)
    
    Computed.Close <- min(Computed.Close,Threshold.Close1)
    
    Forecast <-  head(tail(file.df$Fcst,k),1)
    Computed.Plan <- max(Computed.Close - Prev.Close + Forecast,0)
    
    
    Modeled.Close <- Prev.Close - Forecast + Computed.Plan
    Len = nrow(file.df)
    Len = Len - k +1
    file.df[Len,"Close"] <- Modeled.Close
    file.df[Len,"Plan"] <- Computed.Plan
    file.df[Len, "Cover"] <- Modeled.Close/(FCST4WEEK/4)
    k = k-1
    file.df[Len, "previousclose"] <-  tail(file.df1$Close,1)
  }
  
  file.df1<-tail(file.df,13)
  return(file.df1)
} 
#a<-sku_list[1]
sku_list_1<-list()
j=1
for(i in 1:length(sku_list))
{
  if(nrow(sku_list[[i]])<nrow(sku_list[[1]]))
  {
    
  }
  else
  {
    sku_list_1[[j]]<-sku_list[[i]]
  }
  j=j+1
}

sku_list<-sku_list_1


sku_list_forecast<-lapply(sku_list, Var_model)
sku_list_last13<-do.call(rbind,sku_list_forecast)
#sku_list_last13$Cover
#install.packages(reshape2)
library(reshape2)
library(tidyr)
sku_list_1ast_gather<-gather(sku_list_last13,name,values,9:ncol(sku_list_last13))
sku_list_last13$Week<-NULL
sku_list13_unite<-unite(sku_list_1ast_gather,col = "changelater",name,Date,sep="_")
sku_list13_unite$Plant<-NULL
sku_list13_unite$Week<-NULL
dtry<-dcast(sku_list13_unite,Material~changelater,value.var = c('values'))
dtry<-dtry[,-((ncol(dtry)-12):ncol(dtry))]

dtry<-dtry[,-((ncol(dtry)-12):ncol(dtry))]


colnames_df<-as.data.frame(colnames(dtry)[2:length(colnames(dtry))])
colnames_df<-as.data.frame(do.call(rbind,strsplit(as.character(colnames_df$`colnames(dtry)[2:length(colnames(dtry))]`),'_',fixed = T)))
colnames(colnames_df)<-c("Name","Date")
colnames_df$Date<- format(as.Date(colnames_df$Date,"%d-%m-%Y"),"%d-%m-%Y")
colnames_df<-separate(colnames_df,col=Date,into = c("Day","Month","Year"),remove = F)
colnames_df<-colnames_df[with(colnames_df,order(Year,Month,Day)),]

#colnames_df<-colnames_df[order(colnames_df$Date),]
colnames_df1<-paste0(colnames_df$Name,"_",colnames_df$Date)
colnames_df0<-c("Material")
dtry<-dtry[,c(colnames_df0,colnames_df1)]

colnames(dtry)[which(colnames(dtry)=="Material")]<-"SKU"
#maml.mapOutputPort("dtry");




# found<-c()
# for(i in 1:67)
# {
#   i
# test_1<-which(sku_list[[i]][[1]] == 67627638)
# 
# if(length(test_1) != 0)
# {
# found<-i
# }
# if(length(found)!= 0 )
# break
# }
