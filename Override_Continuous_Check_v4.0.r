                                            ##### MODULE 4 #####
                                            
                              #### CALCULATION FOR OVERRIDE AND CONTINUOUS ####
                                            start_time<-Sys.time()
##### REQUIRED LIBRARIES #####
library(plyr)

##### READING DATA #####

#input_data<- week13

#Pre_Plan_data<-source_data_1[,c(1:(which(start_date_1 == colnames(source_data_1))-1))]
#Pre_Plan_data<-source_data_1[,c(2,grep("^Plan",colnames(source_data_1)))]
source_data_1<-df
Pre_Plan_data<-source_data_1[,c(1,grep("^Plan",colnames(source_data_1)))]
#colnames(Pre_Plan_data)<-Pre_Plan_data[1,]
#Pre_Plan_data<-Pre_Plan_data[-1,]
plan_index<-grep("^Plan",colnames(Pre_Plan_data))


#####   CHECKING IF PLAN WEEK ARE >= ONE #####

if(length(plan_index) >=1)
{
  colnames(Pre_Plan_data)[1]<-"SKU"
  Final_inputdata<-merge(week13[,c(1,3)],Pre_Plan_data[,c(1,plan_index)],by ="SKU",all.x = T)
  plan_index<-grep("^Plan",colnames(Final_inputdata))
  if(length(plan_index) == 1)
  {
    Final_inputdata[,plan_index]<-as.numeric(Final_inputdata[,plan_index])
  }
  if(length(plan_index) > 1)
  {
    Final_inputdata[,plan_index]<-lapply(Final_inputdata[,plan_index],as.numeric)
  }
  Final_inputdata[,plan_index][is.na(Final_inputdata[,plan_index])]<-0
  sku_group_info<-Final_inputdata
  Final_inputdata<-Final_inputdata[,-1]
  Final_inputdata<-ddply(Final_inputdata,.(Group),numcolwise(sum))
  Final_inputdata_MOQ<-ddply(week13[,c(2:3)],.(Group),summarize,MOQ=mean(MOQ))
  Final_inputdata<-merge(Final_inputdata,Final_inputdata_MOQ,by="Group")
  Final_inputdata<-Final_inputdata[!is.na(Final_inputdata$MOQ),]

                          #### ////  COMMENTS //// #######

#############  FUNCTION FOR CALCULATING CONTINUOUS PRODUCING SKU's #############

Continuous_Function = function(df)
  {
    col_names = c(colnames(df))
    #index_Col_names = grep("^Plansum", col_names)
    index_Col_names = grep("^Plan", col_names)
    
    df$weekidforcontinuous <- 'NA'
    df$continous <- 0
    for (i in 1:length(df$Group)) 
    {
      
      if(length(index_Col_names) < 2){
      df$continous[i] = 0
        
      }
      if(length(index_Col_names) >= 2)
      {
        TotPLAN_COL = length(index_Col_names)
        j=1
        
        if((ceiling(df[i, index_Col_names[j]])>0 & ceiling(df[i, index_Col_names[j]]) < df$MOQ[i]) & (ceiling(df[i, index_Col_names[j+1]])>0 & ceiling(df[i, index_Col_names[j+1]])< df$MOQ[i]) | 
           
           (ceiling(df[i, index_Col_names[j]])>0 & ceiling(df[i, index_Col_names[j+1]])>0 & (ceiling(df[i, index_Col_names[j]]) + ceiling(df[i, index_Col_names[j+1]]) < 2* df$MOQ[i])) ) 
        {
          
          while(TotPLAN_COL >= 2)
          {
            
            if((df[i,index_Col_names[j]] + df[i,index_Col_names[j+1]]) >= df$MOQ[i])
            {
              
              df$continous[i] = 1
              df$weekidforcontinuous[i] = colnames(df)[index_Col_names[j]]
            }
            
            j=j+1
            TotPLAN_COL = TotPLAN_COL - 1
          }
        }
        
      }
    }
    return(df)  
  }  
  
######## RUN Continuous_Function AND CALCULATE THE CONTINUOUS PRODUCING SKU's  ######   
  
Over = Continuous_Function(Final_inputdata)
  
                       #### ////  COMMENTS //// #######  

######## FUNCTION FOR CALCULATING OVERRIDE PRODUCING SKU's  ###############
  
Override_Function = function(df)
  {
    
    #df <- gs11
    col_names = c(colnames(df))
    index_number = grep("^Plan", col_names)
    df$OverRide = 0
    df$Count = 0
    row.names(df)<-NULL
    for (i in 1:length(df$Group)) 
    {
      df$OverRide[i] = 0
      df$Count[i] = 0
      
      for (j in index_number) 
      {
        
        
        if((ceiling(df[i,j]) < df$MOQ[i]) & (df[i,j] >0))
        {
          
          #df$OverRide[i] = 1
          df$Count[i] = df$Count[i]+1
          
        }
        else
        {
          
          df$OverRide[i] = 0
        }
        
        
      }
      df$OverRide[i] = ifelse(df$Count[i]>0,1,0)
      
      
    }
    return(df)
    
  }

######## RUN Override_Function AND CALCULATE THE OVERRIDE PRODUCING SKU's  ######  

over_ride = Override_Function(Final_inputdata)
  
length_plan<-length(grep("^Plan" , colnames(Final_inputdata)))
  if(length_plan > 1)
  {
  mod_override<-over_ride[,c("Group","OverRide","Count")]
  merge = merge(Over, mod_override, by = "Group", all.x = TRUE)
  
  
####  CALCULATION OF NEW OVERRIDE  ######

for(i in 1:length(merge$Group))
  {
    merge$New_OverRide[i] =ifelse(merge$Count[i] > 2 & merge$continous[i] > 0,1,
                                  ifelse(merge$Count[i] <= 2 & merge$continous[i]>0,0,
                                         merge$OverRide[i]))
  }
  
  
  
#### CALCULATION OF NEW CONTINUOUS  ######

for(i in 1:length(merge$Group))
  {
    merge$New_Continuous[i]=ifelse(merge$Count[i]>2 & merge$continous[i] >0,1,
                                   ifelse(merge$Count[i] <= 2 & merge$continous[i]>0, 1,
                                          merge$continous[i]))
    
  }
  
  
plan_index_updated<-grep("^Plan",colnames(merge))
merge<-merge[,-c(plan_index_updated)]

######  MERGED DATARAME   #####

change = merge(sku_group_info, merge, by = "Group", all.x = TRUE)
  
######  FILTER DATA WHERE NEW CONTINUOUS == 1 ##########

change=change[!is.na(change$MOQ),]
new_df=change[change$New_Continuous==1 ,]

weekid<-new_df$weekidforcontinuous[1]
currcol_index<-grep(weekid,colnames(new_df))
nextcol_index<-grep(weekid,colnames(new_df))+1

####### ARRANGING DATA IN THE DESCENDING ORDER AT GROUP LEVEL  ####   
new_df =new_df[order(-new_df$Group,-new_df[,nextcol_index]),]
  
new_df1<-ddply(new_df,.(Group),numcolwise(sum))
new_df1<-new_df1[,c("Group",weekid)]
  
final_df = merge(x = new_df, y = new_df1, by = "Group", all.x = TRUE)
colnames(final_df)[ncol(final_df)]<-"sum_week"

###### CUMULATIVE SUM GROUP WISE #########

final_df$csum <- ave(final_df[,nextcol_index], final_df$Group, FUN=cumsum)
  
#########    LOOP FOR CALCULATING FINAL CONTINUOUS    ######

grp = unique(final_df$Group)
updated_df<-data.frame()
  for (i in 1:length(grp)) 
  {
    subset = final_df[final_df$Group == grp[i],]
    
    for (j in 1:nrow(subset))
    {
      if((subset$sum_week[j]+ subset$csum[j]) < subset$MOQ[j])
      { 
        subset$finalcontinuous[j] = 2
      }
      
      if((subset$sum_week[j]+ subset$csum[j]) >= subset$MOQ[j])
      {
        subset$finalcontinuous[j] = 1
        break
      }
      
    }
    if(nrow(subset)>1 & j!=length(subset$finalcontinuous))
    {
      for(k in c(j+1:(length(subset$finalcontinuous)-j)))
      {
        subset$finalcontinuous[k]<-0  
      }
    }
    updated_df<-rbind(updated_df,subset)
  }
  over_ride<-change[,c("SKU","New_OverRide")]
  }
  
  else
  {
    sku_moq<-week13[,c(1,3)]
    over_ride<-merge(sku_moq,over_ride[,c(1,4)],by="Group",all.x=T)
    over_ride<-over_ride[,c("SKU","OverRide")]
  }
  }else
{
  
  SKU<-week13[,1]
  over_ride<-data.frame(SKU,OverRide=0)
  
}
print(Sys.time()-start_time)
####################  CALCULATION OF OVERRIDE AND CONTINUOUS IS DONE ###########################
####################                    END                   ###########################
################# OUTPUT OF THIS MODULE 4  WILL BE INPUT FOR MODULE 5 #####################