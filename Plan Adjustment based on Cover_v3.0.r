                                            #######   MODULE 8 ########
                  #############   PLAN ADJUSTMENT BASED ON COVER AND PREVIOUS OVERRIDE  ############
data_preprocessing<-read.csv("Intermediate Data_.csv")
colnames(data_preprocessing)[c(1,13)]<-c("Date","MinVolProd")
min_quantity<-unique(data_preprocessing[,c("Material","MinVolProd")])
colnames(min_quantity)[1]<-"SKU"
min_quantity[is.na(min_quantity)]<-0
start_time<-Sys.time()
group_mapping<-week13[,c("SKU","SKU_TYPE","INTERCHANGE_SKU_NUMBER","Group")]

                                            
######  REQUIRED LIBRARIES ####### 
#install.packages("dplyr")
library(plyr)
#library(dplyr)
library(lubridate)

                                               
             #DF<-updated                                  
override_function<-function(DF){
  close_cover_update<-function(data){
    fcst.in<-grep("fcst",colnames(data))
    numcol.a<-length(fcst.in)
    for(l in 1:numcol.a)
      
    {
      fcst.in<-grep("fcst",colnames(data))
      plan<-grep("plancheck",colnames(data))
      close<-grep("close.out",colnames(data))
      
      if(l == 1)
      {
        
        data[,close[l]]<-round(data$pre_Close - data[,fcst.in[l]]+ data[,plan[l]],6)
        
      }
      
      if(l > 1)
        
      {
        data[,close[l]]<-round(data[,close[l-1]] - data[,fcst.in[l]]+ data[,plan[l]],6)
        
      }
      
      
    }
    # #computing output cover
    for(m in 1:numcol.a)
    {
      cover.out<-grep("cover.out",colnames(data))
      close.out<-grep("close.out",colnames(data))
      fourwksf<-grep("4wksf",colnames(data))
      data[,cover.out[m]]<-round((data[,close.out[m]]/data[,fourwksf[m]])*4,6)
      
    }
    
    return(data)
  }
  
  
  i=0
  
  subset.moqcheck<-DF[DF$groupsumall13wks >= DF$MOQ,]
  
  
  if(nrow(subset.moqcheck) >= 1)
  {
    uniquegroup<-unique(subset.moqcheck$Group)
    
    override<-data.frame()
    
    for(q in 1:length(uniquegroup))
    {
      group.level<-subset(subset.moqcheck,Group == uniquegroup[q])
      group.level_a<-subset(subset.moqcheck,Group == uniquegroup[q])
      actualgroupsum<-group.level$groupsumall13wks
      actualplansum<-group.level$plan_sum
      inputratio<-group.level$ratio
      
      
      cover.out<-grep("cover.out",colnames(group.level))
      cover.in<-grep("cover.In",colnames(group.level))
      plan.check<-grep("plancheck",colnames(group.level))
      fourwksf<-grep("4wksf",colnames(group.level))
      fcst<-grep("fcst",colnames(group.level))
      k=1
      plansum<-c()
      for(l in 1:length(plan.check))
      {
        plansum[l]<-round(sum(group.level[,plan.check[l]]),6)
      }
      
      actual_ratio=floor(group.level$ratio[1])
      loopcount<-0
      updated_index<-c()
      
      greater<-which(round(plansum,1) >= group.level$MOQ[1])
      if(length(greater)>=1)
      {
        group.level$groupsumall13wks<-group.level$groupsumall13wks-round(sum(group.level[,plan.check[greater]]),6) 
        group.level$ratio<-group.level$groupsumall13wks/group.level$MOQ
        group.level$plan_sum<-group.level$plan_sum-round(rowSums(group.level[plan.check[greater]]),6)
        updated_index<-c(updated_index,greater)
        actual_ratio=floor(group.level$ratio[1])
      }
      if(floor(group.level$ratio[1])>=1){
        repeat{
          
          for(i in 1:length(plan.check))
          {
            
            if(round(sum(group.level[,plan.check[i]]),1) >= group.level$MOQ[1] ) next
            
            if((sum(group.level[,plan.check[i]])) == 0 & all((group.level[,cover.out[i]] >= group.level[,cover.in[i]]),na.rm = T))
            {
              cat("skipping the loop",i,"but updating close and cover")
              group.level<-close_cover_update(group.level)
            }
            if((sum(group.level[,plan.check[i]])) == 0 & all((group.level[,cover.out[i]] >= group.level[,cover.in[i]]),na.rm = T)) next
            
            if((sum(group.level[,plan.check[1]])) != 0 & (round(sum(group.level[,plan.check[i]]),1) < group.level$MOQ[1]) & all((round(group.level[,cover.out[i]],2) >= round(group.level[,cover.in[i]],2)),na.rm = T) | (round(sum(group.level[,plan.check[i]]),1) < group.level$MOQ[1] )& all((round(group.level[,cover.out[i]],2) >= round(group.level[,cover.in[i]],2)),na.rm = T))
            {
              
              print(paste("group", uniquegroup[q]))
              print(paste(i,"st iteration"))
              print("before plan calcu")
              print(group.level[,plan.check[i]])
              print("after calu")
              plan_after_calcu<-group.level$plan_sum/group.level$ratio
              print(plan_after_calcu)
              group.level[,plan.check[i]]<-pmax(group.level$plan_sum/group.level$ratio,group.level_a[,plan.check[i]])
              print("max of them")
              print(group.level[,plan.check[i]])
              print("sum of calculated plan")
              print(sum(group.level[,plan.check[i]]))
              updated_index<-c(updated_index,i)
              
              if(round(sum(group.level[,plan.check[i]]),1) > group.level$MOQ[1])
              {
                print("calculated plan is greater than moq")
                group.level$plan_sum <- actualplansum - rowSums(group.level[plan.check[updated_index]])
                group.level$groupsumall13wks<-actualgroupsum-sum(group.level[,plan.check[updated_index]])
                group.level$ratio<-group.level$groupsumall13wks/group.level$MOQ
                if(floor(group.level$ratio[1]) == 0)break
              }
              
              if(round(sum(group.level[,plan.check[i]]),1) == group.level$MOQ[1])
              {
                print("calculated plan is equal to moq")
                group.level$ratio<-group.level$ratio-1
                group.level$plan_sum<-group.level$plan_sum-group.level[,plan.check[i]]
              }
              
              print("close value before updating:")
              close.out<-grep("close.out",colnames(group.level))
              print(group.level[,close.out[i]])
              print("cover value before updating:")
              cover.out<-grep("cover.out",colnames(group.level))
              print(group.level[,cover.out[i]])
              group.level<-close_cover_update(group.level)
              print("updated close: ")
              print(group.level[,close.out[i]])
              print("updated cover: ")
              print(group.level[,cover.out[i]])
              
              break
            }  
            
          }#for loop          
          
          if(k == actual_ratio)
          {
            cat("ratio",k)
            print("loop broke")
            break
            
          }
          k=k+1
        }#repeat loop
        
      }#if floor value is greater than or equal to 1    
      
      
      else
      {
        if(group.level$ratio[1]!=0)
        {
        remaining<-round(group.level$MOQ[1]*(group.level$ratio[1]),6)
        lesser<-which(plansum < group.level$MOQ[1] & plansum !=0 )
        group.level[,plan.check[lesser[1]]]<-group.level$plan_sum
        group.level <-close_cover_update(group.level)
        group.level$groupsumall13wks<-actualgroupsum
        plan.check<-grep("plancheck",colnames(group.level))
        usedcolumns<-c(lesser[1],greater)
        unusedcol<-which(!(c(1:length(plansum)) %in% usedcolumns))
        group.level[,plan.check[unusedcol]]<-0
        
        i=0
        remaining<-0
        }
      }
      
      #Checking if all the weeks are updated or not 
      if(length(updated_index) != length(plan.check))
      {
        print("remaining calc")
        plan.check<-grep("plancheck",colnames(group.level))
        cover.out<-grep("cover.out",colnames(group.level))
        unusedcol<-which(!(c(1:length(plan.check)) %in% updated_index))
        
        
        plansum_unused_col<-round(colSums(group.level[,plan.check]),6)
        
        zero_index<-which(plansum_unused_col == 0 )
        
        if(length(zero_index)>=1){
          if(zero_index[1] == 1 & all((round(group.level[,cover.out[1]],2) >= round(group.level[,cover.in[1]],2)),na.rm = T))
          {
            zero_index<-zero_index[-zero_index[1]]
            }
          
          print("zero index removal")
          if(length(zero_index)>=1){
          for(x in 1:length(zero_index))
          {
            
            if(all((round(group.level[,cover.out[zero_index[x]]],2) >= round(group.level[,cover.in[zero_index[x]]],2)),na.rm = T))
            {
              col_to_be_removed<-which(zero_index[x] == unusedcol)
              print("unused col removed")
              unusedcol<-unusedcol[-col_to_be_removed]
              print(unusedcol)
              unusedcol<-unusedcol
            }
            if(all((round(group.level[,cover.out[zero_index[x]]],2) < round(group.level[,cover.in[zero_index[x]]],2)),na.rm = T))
            {
              print("do nothing")
            }
            
          }
          }
        }
        if(length(unusedcol) != 0){
        plan_lesser<-which(round(plansum_unused_col[unusedcol],2) < group.level$MOQ[1])
        updated_plans<-rowSums(group.level[plan.check[updated_index]])
        group.level[,plan.check[unusedcol[plan_lesser[1]]]]<-actualplansum-updated_plans
        group.level<-close_cover_update(group.level)
        updated_index<-c(updated_index,unusedcol[plan_lesser[1]]) }   
      }
      else #if all are updated then do nothing. 
      {
        print("all week plans are updated. ")
      }
      
      
      
      
      unusedcol<-which(!(c(1:length(plansum)) %in% updated_index))
      group.level[,plan.check[unusedcol]]<-0
      group.level<-close_cover_update(group.level)
      
      group.level$groupsumall13wks<-actualgroupsum
      group.level$plan_sum<-actualplansum
      group.level$ratio<-inputratio
      override<-rbind(override,group.level)
      b=0
    }
    
    
    override<-close_cover_update(override)
    result_override<-override
    result_skunotinoverride<-DF[!(DF$SKU%in%override$SKU) ,]
    Final_df<-rbind(result_override,result_skunotinoverride)
    
  }else #entry loop
  {
    Final_df<-DF
  }
  return(Final_df)
}

b=0

close_cover_update<-function(data)
{
  
  
  for(l in 1:numcol.a)
    
  {
    fcst.in<-grep("fcst",colnames(data))
    plan<-grep("plancheck",colnames(data))
    
    if(l == 1)
    {
      
      data[paste0("close.out",l)]<-round(data$pre_Close - data[,fcst.in[l]]+ data[,plan[l]],6)
      
    }
    
    else
      
    {
      data[paste0("close.out",l)]<-round(data[,paste0("close.out",l-1)] - data[,fcst.in[l]]+ data[,plan[l]],6)
      
    }
    
    
  }
  
  # #computing output cover
  for(m in 1:numcol.a)
  {
    name_cover.out<-paste0("cover.out",m)
    close.out<-grep("close.out",colnames(data))
    fourwksf<-grep("4wksf",colnames(data))
    data[name_cover.out]<-round((data[,close.out[m]]/data[,fourwksf[m]])*4,6)
    
  }
  return(data)
}
##################################################week_month identifier########################################
week_month_identifier<-function(dateseq)
{
  month_identifier<-function(weekNum)
  {
    startDate=as.Date(paste(2018, weekNum-1, 1, sep="-"), "%Y-%U-%u")
    startDate
    endDate= startDate + days(7)
    myDates <-seq(from = startDate, to = endDate, by = "days")
    weekstart=myDates[wday(myDates)==5]
    weekstart
    m=month(weekstart)
    n=month(startDate)
    
    monthname<-ifelse(month(weekstart)==month(startDate), toString(month(startDate, label = TRUE)), toString(month(endDate, label = TRUE)))
    monthnumeber<-match(monthname,month.abb)
    return(monthnumeber)
  }
  
  monthvec<-c()
  monthvec1<-c()
  for(i in 1:length(dateseq))
  {
    
    monthvec[i]=month_identifier(week(dateseq[i]))
    monthvec1[i]<-ifelse(monthvec[i]<10,paste0("0",monthvec[i]),monthvec[i])
  }
  
  return(monthvec1)
}

###############################################################################################################

data<-week13[,-c(5,6)]
colnames_data<-colnames(data)
data<-as.data.frame(lapply(data,as.numeric))
colnames(data)<-colnames_data
empty_moq<-data[is.na(data$MOQ),]
#empty_moq<-data[which(data$MOQ == 0),]

data<-data[!is.na(data$MOQ),]
#data<-data[-which(data$MOQ == 0),]
data_1<- data
week13_df<-data[,c(1:3)]
week13_cap<-week13[,c(1:3,5:6)]
empty_moq_merge<-week13[,c(1:3,5:6)]

date.seq <- seq.Date(as.Date(start_date_2), as.Date(end_date_2), by="week")
date.seq<-as.character(date.seq)



###################################################################################################
#all 13wks plan_sum and group_plan_sum

#plan_data<-select(data_1,"SKU","Group","MOQ",contains("plan"))
plan_data<-data_1[,c((which(colnames(data_1) == "SKU" | colnames(data_1) == "Group"|colnames(data_1) == "MOQ")),grep("plan",colnames(data_1)))]
plan_data$plan_sum<-apply(plan_data[,c(4:16)],1,FUN = sum)

group_sum<-ddply(plan_data,.(Group),summarize,groupsumall13wks=sum(plan_sum))
plan_data<-merge(plan_data,group_sum,by="Group")
plan_data$ratio_groupsumplan_MOQ<- plan_data$groupsumall13wks/plan_data$MOQ

colnames(plan_data)[4:16]<-paste0(date.seq,"/",week_month_identifier(date.seq))
plan13wksdata<-plan_data

###############################################################################################

unique_months<-unique(week_month_identifier(date.seq))
months<-week_month_identifier(date.seq)

for(i in 1:length(unique_months))
#for(i in 1)
{
  date.seq_1<-months[which(months == unique_months[i])]
  
  month_wise_data<-plan_data[,c(1:3,which(substr(colnames(plan_data),12,13) == unique_months[i]))] # taking plan_data and other required info also
  
  month_wise_data[is.na(month_wise_data)]<-0
  
  if(ncol(month_wise_data)>4)
  {
    month_wise_data$plan_sum<-round(apply(month_wise_data[,c(4:ncol(month_wise_data))],1,FUN = sum),6)
  }
  if(ncol(month_wise_data)==4)
  {
    month_wise_data$plan_sum<-month_wise_data[,4]
  }
  group_sum_1<-ddply(month_wise_data,.(Group),summarize,groupsumall13wks=round(sum(plan_sum),6)) # plansum calculation month wise
  month_wise_data<-merge(month_wise_data,group_sum_1,by="Group")#groupsum calculation month wise
   
  if(i == 1)
  {
    month_wise_data<-merge(month_wise_data,over_ride[,c(1,grep("OverRide",colnames(over_ride)))],by="SKU",all.x=T)
    colnames(month_wise_data)[ncol(month_wise_data)]<-"PREVIOUS_OVERIDE"
  }
  
  
  #subsetting based on the condition
  if(i != 1)
  {
    month_wise_data$PREVIOUS_OVERIDE<-0
  }
  
  
  subset.moqcheck<-month_wise_data[month_wise_data$groupsumall13wks < month_wise_data$MOQ & month_wise_data$groupsumall13wks !=0 ,]
  numcol.a<-ncol(month_wise_data)-6
  
  
  #fourwksfcst
  #fourwksfcst<-select(data,"SKU","Group","close.out0",contains("4wksf"))
  fourwksfcst<-data[,c((which(colnames(data) == "SKU" | colnames(data) == "Group"|colnames(data) == "close.out0")),grep("4wksf",colnames(data)))]
  fourwksfcst<-fourwksfcst[,c(1,3,which(substr(colnames(fourwksfcst),18,19) == unique_months[i]))]
  merge_df<-merge(subset.moqcheck,fourwksfcst,by.x = "SKU") #left join , taking data only for subsetted values. 
  month_wise_data<-merge(month_wise_data,fourwksfcst,by.x = "SKU")
  fourwksf.in<-grep("4wksf",colnames(merge_df))
  fourwksf.in1<-grep("4wksf",colnames(month_wise_data))
  colnames(month_wise_data)[fourwksf.in1[1]-1]<-"pre_Close"
  colnames(merge_df)[fourwksf.in1[1]-1]<-"pre_Close"
  
  #updating pre_close
  if(i != 1)
  {
    close.out_updated<-grep("close.out",colnames(updated))
    latest_close<-updated[,c(1,close.out_updated[length(close.out_updated)])]
    month_wise_data<-merge(month_wise_data,latest_close,by="SKU")
    month_wise_data$pre_Close<-month_wise_data[,ncol(month_wise_data)]
    month_wise_data<-month_wise_data[,-ncol(month_wise_data)]
    
    merge_df<-merge(merge_df,latest_close,by="SKU")
    merge_df$pre_Close<-merge_df[,ncol(merge_df)]
    merge_df<-merge_df[,-ncol(merge_df)]
    
  }
  
  #cover
  cover<-data[,c((which(colnames(data) == "SKU")),grep("cover",colnames(data)))]
  cover<-cover[,c(1,which(substr(colnames(cover),21,22) == unique_months[i]))]
  merge_df_cover<-merge(merge_df,cover,by.x = "SKU")
  month_wise_data<-merge(month_wise_data,cover,by.x = "SKU")
  
  #fcst
  fcst<-data[,c((which(colnames(data) == "SKU")),grep("fcst",colnames(data)))]
  fcst<-fcst[,c(1,which(substr(colnames(fcst),17,18) == unique_months[i]))]
  merge_df_final<-merge(merge_df_cover,fcst,by.x = "SKU") #merge_df_final is subsetted data based on groupsum < moq
  month_wise_data<-merge(month_wise_data,fcst,by.x = "SKU") # a is subsetted data based on month
  
  #initialising plan values to 0
  for(j in 1:numcol.a)
  {
    name<-paste0("plancheck",j)
    merge_df_final[name] <- 0
    month_wise_data[name]<-0
  }
  
  for(k in 1:numcol.a)
  {
    #computingclose.In
    name_close<-paste0("close.In",k)
    fourwksf.in<-grep("4wksf",colnames(merge_df_final))
    cover.in<-grep("cover",colnames(merge_df_final))
    merge_df_final[name_close]<-round(merge_df_final[,cover.in[k]]*(merge_df_final[,fourwksf.in[k]]/4),6)
    month_wise_data[name_close]<-month_wise_data[,cover.in[k]]*(month_wise_data[,fourwksf.in[k]]/4)
  }
  
  ############################################################  
  #computing close when plan values are equal to 0
  for(l in 1:numcol.a)
  {
    fcst.in<-grep("fcst",colnames(merge_df_final))
    plan<-grep("plancheck",colnames(merge_df_final))
    name_close.out<-paste0("close.out",l)
    
    if(l == 1 & i == 1)
    {
      #computing close.out when plan is equal to zero
      merge_df_final[name_close.out]<-merge_df_final[,which(substr(colnames(merge_df_final),5,9) == "Close")]-merge_df_final[,fcst.in[l]]+ merge_df_final[,plan[l]]
      month_wise_data[name_close.out]<-month_wise_data[,which(substr(colnames(month_wise_data),5,9) == "Close")]-month_wise_data[,fcst.in[l]]+month_wise_data[,plan[l]]
    }
    
    if(l != 1 & i == 1)
    {
      merge_df_final[name_close.out]<-merge_df_final[,ncol(merge_df_final)] - merge_df_final[,fcst.in[l]]+ merge_df_final[,plan[l]]
      month_wise_data[name_close.out]<-month_wise_data[,ncol(month_wise_data)] - month_wise_data[,fcst.in[l]]+ month_wise_data[,plan[l]]
    }
    
    #code for second month(i=2).
    if(i>1 & l==1)
    {
      merge_df_final[name_close.out]<-merge_df_final[,which(substr(colnames(merge_df_final),5,9) == "Close")]-merge_df_final[,fcst.in[l]]+ merge_df_final[,plan[l]]
      month_wise_data[name_close.out]<-month_wise_data[,which(substr(colnames(month_wise_data),5,9) == "Close")]+month_wise_data[,plan[l]]
      
    }
    if(i>1 & l!=1)
    {
      merge_df_final[name_close.out]<-merge_df_final[,ncol(merge_df_final)] - merge_df_final[,fcst.in[l]]+ merge_df_final[,plan[l]]
      month_wise_data[name_close.out]<-month_wise_data[,ncol(month_wise_data)] - month_wise_data[,fcst.in[l]]+ month_wise_data[,plan[l]]
      
    } 
  }
  ###############################################
  # #computing output cover
  
  
  for(m in 1:numcol.a)
  {
    name_cover.out<-paste0("cover.out",m)
    close.out<-grep("close.out",colnames(merge_df_final))
    fourwksf<-grep("4wksf",colnames(merge_df_final))
    merge_df_final[name_cover.out]<-(merge_df_final[,close.out[m]]/merge_df_final[,fourwksf[m]])*4
    month_wise_data[name_cover.out]<-(month_wise_data[,close.out[m]]/month_wise_data[,fourwksf[m]])*4
  }
  
  dummy1<-data.frame()
  dummy<-data.frame()
  uniquegroup<-unique(merge_df_final$Group)
  for(q in 1:length(uniquegroup))
  {
    group.level<-subset(merge_df_final,Group == uniquegroup[q])
    for(j in 1:numcol.a)#for all week column
    {
      
      for(n in 1:nrow(group.level))#for all skus(rows)
        
      { 
        cover.out<-grep("cover.out",colnames(group.level))
        plan.check<-grep("plancheck",colnames(group.level))
        
        fcst<-grep("fcst",colnames(group.level))
        fourwks<-grep("4wksf",colnames(group.level))
        close.in<-grep("close.In",colnames(group.level))
        plan<-grep("plan",colnames(group.level))
        plan.check<-grep("plan.check",colnames(group.level))
        close.out<-grep("close.out",colnames(group.level))
        
        plan.check<-grep("plancheck",colnames(merge_df_final))
        if(group.level$PREVIOUS_OVERIDE[n] == 1 & (!(is.na(group.level[n,cover.out[j]])) & !(is.infinite(group.level[n,cover.out[j]]))) & (group.level[n,cover.out[j]] < 2.5))
        {
          group.level[,plan.check[j]] <- group.level$plan_sum
          dummy<-rbind(dummy,group.level)
          break
        } 
        
        
        if(group.level$PREVIOUS_OVERIDE[n] == 0  & (!(is.na(group.level[n,cover.out[j]]))&!(is.infinite(group.level[n,cover.out[j]]))) & (group.level[n,cover.out[j]] < group.level[n,cover.in[j]]))
        {
          group.level[,plan.check[j]] <- group.level$plan_sum
          dummy1<-rbind(dummy1,group.level)
          break
        } 
        
      }
      if(group.level$PREVIOUS_OVERIDE[n] == 1 & (!(is.na(group.level[n,cover.out[j]])) & !(is.infinite(group.level[n,cover.out[j]]))) & (group.level[n,cover.out[j]] < 2.5))
        break
      if(group.level$PREVIOUS_OVERIDE[n] == 0 & (!(is.na(group.level[n,cover.out[j]])) & !(is.infinite(group.level[n,cover.out[j]]))) & (group.level[n,cover.out[j]] < group.level[n,cover.in[j]]))
        break
    }
  }
  
  result_df<-rbind(dummy,dummy1)
  result_df1<-month_wise_data[!(month_wise_data$SKU%in%result_df$SKU) ,]
  for(j in 1:numcol.a)
  {
    plan.check<-grep("plancheck",colnames(result_df1))
    result_df1[,plan.check[j]]<-result_df1[,j+3]
  }
  result<-rbind(result_df,result_df1)
  updated<-close_cover_update(result) # this data frame has to be used to compute close and cover values of the next month(from 2nd month)
  updated$ratio<-ifelse(updated$groupsumall13wks > updated$MOQ,updated$groupsumall13wks/updated$MOQ,0)
  updated<-override_function(updated)
  nam1 <- paste("new","month",i)
  assign(nam1,updated)
  updated<-minquantity_function(updated)
  nam1 <- paste("minquant",i)
  assign(nam1,updated)
  
  plancheck_index<-grep("plancheck",colnames(updated))
  if(length(plancheck_index) > 1)
  {
  cap<-capacity_check(updated)
  cap<-cap[,c(1:5,grep("cover.In",colnames(cap)),grep("4wksf",colnames(cap)),grep("fcst",colnames(cap)),grep("close.In",colnames(cap)),grep("plancheck",colnames(cap)),grep("close.out",colnames(cap)),grep("cover.out",colnames(cap)))]
  nam1 <- paste("capacity_check",i)
  assign(nam1,cap)
  }
  else
  {
    firstfew<-week13[,c(1:3,5:6)]
    cap<-merge(firstfew,updated[,c(1,4:ncol(updated))])
    cap<-cap[,c(1:5,grep("cover.In",colnames(cap)),grep("4wksf",colnames(cap)),grep("fcst",colnames(cap)),grep("close.In",colnames(cap)),grep("plancheck",colnames(cap)),grep("close.out",colnames(cap)),grep("cover.out",colnames(cap)))]
    nam1 <- paste("capacity_check",i)
    assign(nam1,cap)
  }

  monthly_dataframe_3<-updated[,c(1:3,grep("cover.In",colnames(updated)),grep("4wksf",colnames(updated)),grep("fcst",colnames(updated)),grep("close.In",colnames(updated)),grep("plancheck",colnames(updated)),grep("close.out",colnames(updated)),grep("cover.out",colnames(updated)))]
  #monthly_dataframe_3<-select(updated,c(1:3),contains("cover.In"),contains("4wksf"),contains("fcst"),contains("close.In"),contains("plancheck"),contains("close.out"),contains("cover.out"))
  date.seq_new<-rep(date.seq_1,times = 7)
  
  colnames<-c("cover.In","4wksf","fcst","close.In","plancheck","close.out","cover.out")
  colnames_monthly<-rep(colnames,each = length(fourwksf))
  colnames_new<-c()
  for(j in 1:length(colnames_monthly))
  {
    colnames_new[j]<-paste0(colnames_monthly[j],"_",date.seq_new[j])
  }
  colnames(monthly_dataframe_3)[4:length(monthly_dataframe_3)]<-colnames_new
  
  week13_df<-merge(week13_df,monthly_dataframe_3[,c(1,4:ncol(monthly_dataframe_3))],by="SKU")
  
  week13_cap<-merge(week13_cap,cap[,c(1,6:ncol(cap))],by="SKU")
  
}

week13_df<-merge(week13_df,group_mapping,by="SKU",all.x = T)
week13_df<-week13_df[,c(1:3,(ncol(week13_df)-2):(ncol(week13_df)-1),4:(ncol(week13_df)-3))]
empty_moq_merged<-merge(empty_moq_merge,empty_moq[,c(1,5:ncol(empty_moq))],by = "SKU", all.y = T)
colnames(empty_moq_merged)<-colnames(week13_cap)

empty_moq_merged[,grep("plancheck",colnames(empty_moq_merged))]<-0
empty_moq_merged[,c(grep("close",colnames(empty_moq_merged)))]<-NA
empty_moq_merged[,c(grep("cover",colnames(empty_moq_merged)))]<-NA

week13_cap_updated<-rbind(week13_cap,empty_moq_merged)
output<-week13_cap_updated[,c(1:5,grep("plancheck",colnames(week13_cap_updated)),grep("close.out",colnames(week13_cap_updated)),grep("cover.out",colnames(week13_cap_updated)))]

summ<-c()
plan_index1<-grep("plancheck",colnames(output))
for(i in 1:length(plan_index1))
{
  summ[i]<-sum(output[,plan_index1[i]],na.rm=T)
}
print(summ)

#################################################################################################################

colnames(output)[grep("plancheck",colnames(output))]<-paste0("plan","_",c(1:13))
colnames(output)[grep("close",colnames(output))]<-paste0("close","_",c(1:13))
colnames(output)[grep("cover",colnames(output))]<-paste0("cover","_",c(1:13))

colnames(data_preprocessing)[12]<-"cost_per_ton"
input<-unique(data_preprocessing[,c("Material","cost_per_ton")])
colnames(input)[1]<-"SKU"
#input<-read.csv("C:\\Users\\NidhiS\\Desktop\\Testing files\\Week32_44\\New folder\\Stock cover target_1.csv", header=TRUE,stringsAsFactors = F)
output_1<-merge(input[,c(1:2)], output, by="SKU",all.y = T)
colnames(output_1)[2]<-"cost_per_ton"
#factor(output_1$cost_per_ton)

output_1$cost_per_ton<-as.character(output_1$cost_per_ton)
output_1$cost_per_ton<-as.numeric(output_1$cost_per_ton)
output_1$cost_per_ton[is.na(output_1$cost_per_ton)]<-0

for(l in 1:13)
{
  output_1[paste0("stock_val_",l)]<-output_1$cost_per_ton * output_1[,paste0("close_",l)]
  output_1[,paste0("plan_",l)]<-ifelse(output_1$SKU_TYPE=="delist",0,output_1[,paste0("plan_",l)])
  output_1[,paste0("cover_",l)]<-ifelse(output_1$SKU_TYPE=="delist",NA,output_1[,paste0("cover_",l)])
  output_1[,paste0("cover_",l)]<-ifelse(output_1$SKU_TYPE=="make to order",NA,output_1[,paste0("cover_",l)])
  output_1[,paste0("stock_val_",l)]<-ifelse((output_1$SKU_TYPE=="make to order") & (output_1[,paste0("stock_val_",l)]==0 | is.na(output_1[,paste0("stock_val_",l)])),NA,pmax(output_1[,paste0("stock_val_",l)],0))
}

#output_2<-output_1
output_1$SKU_TYPE<-as.character(output_1$SKU_TYPE)
output_1$SKU_TYPE<-ifelse(!(output_1$SKU_TYPE=="make to order"),"Regular",output_1$SKU_TYPE)

output_1<-do.call(data.frame,lapply(output_1, function(x) replace(x, is.infinite(x),NA)))

summ<-c()
summ_stock<-c()
plan_index1<-grep("plan",colnames(output_1))
for(i in 1:length(plan_index1))
{
  summ[i]<-sum(output_1[,plan_index1[i]],na.rm=T)
  stock_index1<-grep("stock",colnames(output_1))
  summ_stock[i]<-sum(output_1[,stock_index1[i]],na.rm=T)
}
print(summ)
print(summ_stock)


output_1<-output_1[,c(1,4,5,3,grep("plan",colnames(output_1)),grep("cover",colnames(output_1)),grep("stock",colnames(output_1)))]
colnames(output_1)[grep("plan",colnames(output_1))]<-paste0("PLAN","_",c(week(start_date_2):week(end_date_2)))
colnames(output_1)[grep("cover",colnames(output_1))]<-paste0("COVER","_",c(week(start_date_2):week(end_date_2)))
colnames(output_1)[grep("stock",colnames(output_1))]<-paste0("STOCK","_",c(week(start_date_2):week(end_date_2)))

#AZURE
#colnames(output_1)[5:ncol(output_1)]<-rep(paste0("WK",c(week(start_date_2):week(end_date_2))),3)

#colnames(output_1)[5:ncol(output_1)]<-rep(paste0("WK",c(week(start_date_2):week(end_date_2))),3)

output_1_new<-output_1
#as.numeric(output_1_new)
output_1_new[,-(1:4)]<-round(output_1_new[,-(1:4)],1)
output_1_new<-output_1_new[order(output_1_new$Group),]
print(Sys.time()-start_time)

#write.csv(output_1_new,"output_1_newformat_new.csv")
format(output_1_new, width = name.width, justify = "centre")
